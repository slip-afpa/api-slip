import datetime
import mysql.connector

"""
mysql.connector.ProgrammingError
mysql.connector.NotSupportedError
mysql.connector.IntegrityError
mysql.connector.OperationalError
mysql.connector.DatabaseError
mysql.connector.InterfaceError
"""


_BDD_OK = True 

def jsonSerializeDateTime(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


def _getParamBDD(infoBDD):
    params = {
        "host": infoBDD['db']['host'],
        "user": infoBDD['db']['user'],
        "password": infoBDD['db']['pass'],
        "database": infoBDD['db']['base'],
        "port": infoBDD['db']['port']
    }
    return params

def checkBDD(infoBDD):
    global _BDD_OK 
    try:
        params = _getParamBDD(infoBDD)
        conn = mysql.connector.connect(**params) 
        conn.close()
        _BDD_OK=True
        return _BDD_OK
    except Exception as e:
        _BDD_OK=False
        return _BDD_OK
    
def fetchBDD(requestSQL, infoBDD):
    """fetch the datas in the bdd

    Args:
        requestSQL (str): request SQL
        infoBDD : DB information

    Returns:
        list: return the datas of the request SQL
    """

    global _BDD_OK 
    datas = []

    if _BDD_OK == False:
        print('connection BDD impossible')
        return datas

    try:
        params = _getParamBDD(infoBDD)
        conn = mysql.connector.connect(**params)
        cursor = conn.cursor(dictionary=True, prepared=False)
        #print( 'req F:', requestSQL)
        cursor.execute(requestSQL)
        datas = cursor.fetchall()
        
    except mysql.connector.InterfaceError as e:
        print('connection BDD impossible')
        _BDD_OK = False 
                
    except Exception as e:
        print( 'WWWWW')
        print( str(e))
        raise( e )

    finally:
        if 'conn' in locals():
            conn.close()
    return datas


def executeBDD(requestSQL, infoBDD):
    """execute one request SQL without fetch of data ( like : DROP, INSERT, UPDATE, etc )

    Args:
        requestSQL (str): request SQL
    """
    global _BDD_OK 


    if _BDD_OK == 'False':
        print('connection BDD impossible')
        return;

    try:
        params = _getParamBDD(infoBDD)
        conn = mysql.connector.connect(**params)
        cursor = conn.cursor(prepared=False)
        print( 'req E:', requestSQL)
        cursor.execute(requestSQL)
        conn.commit()
    except mysql.connector.errors.DatabaseError as e:
        print('connection BDD impossible')
        _BDD_OK = False 

    except Exception as e:
        raise( e )

    finally:
        if 'conn' in locals():
            conn.close()

def getBDDState():
    global _BDD_OK
    return _BDD_OK
