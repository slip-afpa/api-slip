#!/bin/bash

echo ""
echo ""
echo "  Slip installer"
echo "-------------------------------------------------------------------------"
echo "license: MIT"
echo "sourcecode: https://gitlab.com/slip-afpa/api-slip.git"
echo "-------------------------------------------------------------------------"
echo ""


# installing dependecies
pip install mysql.connector
pip install flask
pip install flask_cors

# create the file config.json 
cp config.json.example config.json


echo ""
echo ""
echo "  Installation terminée"
echo "-------------------------------------------------------------------------"
echo "configurer le fichier config.json avec votre édteur favoris"
echo "pour lancer le serveur lancer la commande:  python3 main.py"
echo "-------------------------------------------------------------------------"
echo ""

