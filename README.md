# Api Slip

API du projet SLIP

## required 

- an database installed and created
- Python3 

## Installation

```commandline
pip install mysql.connector
pip install flask
pip install flask_cors
mysql -p nameDatabase < slip.sql

```

## Create your config.json

```commandline
cp config.json.example config.json
vi config.json
```

## Start the server

```commandline
./main.py
```

## Usage
![Usage](https://gitlab.com/slip-afpa/api-slip/-/wikis/images/usage.png)
