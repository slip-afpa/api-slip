"""
S.L.I.P

Peter Fontaine <contact@peterfontaine.fr>
"""
import enum


class SlipLogLevel(enum.IntEnum):
    """
    Enum for Log Level
    """
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40
    FATAL = 50
