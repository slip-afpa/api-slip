-- MariaDB dump 10.19  Distrib 10.5.15-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: slip
-- ------------------------------------------------------
-- Server version	10.5.15-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `files`
--
DROP DATABASE IF EXISTS SLIP;
CREATE DATABASE SLIP;
USE SLIP;

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `author` varchar(256),
  `type` varchar(256),
  `status` enum('ok','KO'),
  `del` BOOLEAN NOT NULL default 0,
  `date` datetime DEFAULT current_timestamp(),
  `modifDate` datetime DEFAULT current_timestamp(),
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`name`, `author`, `type`, `status`, `del`, `date`, `modifDate`) VALUES 
  ('test0.html','anonymous','html','ok',1,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test1.html','anonymous','html','ok',1,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test2.html','anonymous','html','ok',1,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test3.html','anonymous','html','KO',0,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test4.html','anonymous','html','ok',0,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test5.html','anonymous','html','ok',0,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test6.html','anonymous','html','KO',1,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test7.html','anonymous','html','KO',0,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test8.html','anonymous','html','ok',1,'2022-06-10 13:46:59','2022-06-10 13:46:59' ),
  ('test9.html','anonymous','html','ok',0,'2022-06-10 13:46:59','2022-06-10 13:46:59' );



DROP TABLE IF EXISTS `observer`;


CREATE TABLE `observer` (
  `id` int(11) AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `repIN` varchar(256) DEFAULT '',
  `repPROD` varchar(256) DEFAULT '',
  `repERR`  varchar(256) DEFAULT '',
  `status` varchar(256)  DEFAULT '', /* ('start','stop', 'run', 'save', 'read', 'update') */
  `createDate` datetime DEFAULT current_timestamp(),
  `comment` varchar(256),
  `changeDate` datetime DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  `fileCreationDate` datetime ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `observer` (`name`, `repIN`, `repPROD`, `repERR`, `status`,  `createDate`, `comment`) VALUES
  ('racine HTML', '/var/www/html/filesIN','/var/www/html/filesPROD','/var/www/html/filesERR','start','2022-06-10 13:46:59', "répertoire de base pour tester le manager et l'observer" );

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `level` enum('info','warning','debug','error','fatal') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `log` (`message`, `date`, `level`) VALUES ('test','2022-06-10 13:35:14','debug');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-10 13:49:54


