DROP TABLE IF EXISTS `observer`;


CREATE TABLE `observer` (
  `id` int(11) AUTO_INCREMENT,
  `nom` varchar(256) NOT NULL,
  `IN` varchar(256) DEFAULT '',
  `OK` varchar(256) DEFAULT '',
  `KO`  varchar(256) DEFAULT '',
  `status` varchar(256)  DEFAULT '', /* ('start','stop', 'run', 'save', 'read', 'update') */
  `createDate` datetime DEFAULT current_timestamp(),
  `comment` varchar(256),
  `changeDate` datetime DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  `fileCreationDate` datetime ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `observer` (`nom`, `IN`, `OK`, `KO`, `status`,  `createDate`, `comment`) VALUES
  ('racine HTML', '/var/www/html/filesIN','/var/www/html/filesPROD','/var/www/html/filesERR','start','2022-06-10 13:46:59', 'répertoire de base pour tester la BDD' );