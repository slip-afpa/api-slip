import slip_bdd




class Manager():
    def __init__( self, environement ):
        self.environement = environement 
        self.listObservers = []

    def loadListObserver():
        liste = fetchBDD( "SELECT * FROM observer", self.environement)
        for observerData in liste:
            obs = Observer( observerData['id'], 
                            observerData['nom'], 
                            observerData['IN'], 
                            observerData['OK'], 
                            observerData['KO'], 
                            observerData['comment'] 
                             )
            self.listObservers.append( obs )


    def addObserver( obs ):
        executeBDD( obs.create(), self.environement)
        self.listObservers.append( obs )


man = Manager( confData )
man.loadListObserver()


